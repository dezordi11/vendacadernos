/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class ProdutoCaderno implements Serializable{

        private int idProduto;
        private float preco;
        private String nomeProduto;
        private int altura;
        private int largura;
        private int quantidade;

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float preco) {
        this.preco = preco;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }

    public int getLargura() {
        return largura;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
        
public int inserirProduto(ProdutoCaderno produtoCaderno) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO produtoCaderno"
                + "(idProduto, preco, nomeProduto, altura, largura, quantidade) VALUES"
                + "(seq_produtoCaderno_idProduto.nextval,?,?,?,?,?)";

        try {

            String generatedColumns[] = {"idProduto"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setFloat(1, produtoCaderno.getPreco());
            ps.setString(2, produtoCaderno.getNomeProduto());
            ps.setInt(3, produtoCaderno.getAltura());
            ps.setInt(4,produtoCaderno.getLargura());
            ps.setInt(5, produtoCaderno.getQuantidade());

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into seq_produtoCaderno_idprodutoCaderno table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            produtoCaderno.setIdProduto(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idProduto;
    }    
 public static ArrayList<ProdutoCaderno> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<ProdutoCaderno> produtoLista = new ArrayList<>();
        String selectSQL = "SELECT * FROM produtoCaderno";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                ProdutoCaderno p = new ProdutoCaderno();
                p.setIdProduto(rs.getInt("idProduto"));
                p.setPreco(rs.getFloat("preco"));
                p.setNomeProduto(rs.getString("nomeProduto"));
                p.setAltura(rs.getInt("altura"));
                p.setLargura(rs.getInt("largura"));
                p.setQuantidade(rs.getInt("quantidade"));
                produtoLista.add(p);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return produtoLista;
    }
    
    public void getOne(int i){
        this.idProduto = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        String selectSQL = "SELECT * FROM produtoCaderno WHERE idProduto = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.idProduto);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setIdProduto(rs.getInt("idProduto"));
                this.setPreco(rs.getFloat("preco"));                
                this.setNomeProduto(rs.getString("nomeProduto"));
                this.setAltura(rs.getInt("altura"));
                this.setAltura(rs.getInt("largura"));
                this.setQuantidade(rs.getInt("quantidade"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    
    public void update(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        PreparedStatement ps;
        
        String updateTableSQL = "UPDATE produtoCaderno set preco = ?, nomeProduto = ?, altura = ?, largura = ?, quantidade = ? where idProduto = ?";
        try{
            ps = dbConnection.prepareStatement(updateTableSQL);
            
            ps.setFloat(1, this.preco);
            ps.setString(2, this.nomeProduto);
            ps.setInt(3, this.altura);
            ps.setInt(4, this.largura);
            ps.setInt(5, this.quantidade);
            ps.setInt(6, this.idProduto);
            ps.executeUpdate();
            System.out.println("Produto " + this.nomeProduto + " atualizado.");
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
    }
    public boolean delete(){
     Conexao c = new Conexao();
     Connection dbConnection = c.getConexao();
     PreparedStatement ps= null;
     String insertTableSQL = "DELETE FROM ProdutoCaderno WHERE idProduto = ?";
     try{
        ps = dbConnection.prepareStatement(insertTableSQL);
        ps.setInt(1, this.idProduto);
        ps.executeUpdate();
     } catch (SQLException e){
        e.printStackTrace();}
     finally{ c.desconecta();}
     return true;
     }
    


    @Override
    public String toString() {
        return nomeProduto;
    }
           
}

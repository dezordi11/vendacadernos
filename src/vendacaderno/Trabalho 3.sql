
/* Drop Triggers */

DROP TRIGGER TRI_produtoCaderno_idProduto;
DROP TRIGGER TRI_produto_idProduto;
DROP TRIGGER TRI_VendaCaderno_idVenda;
DROP TRIGGER TRI_Venda_idVenda;



/* Drop Tables */

DROP TABLE ItemVendaCaderno CASCADE CONSTRAINTS;
DROP TABLE VendaCaderno CASCADE CONSTRAINTS;
DROP TABLE ClienteCaderno CASCADE CONSTRAINTS;
DROP TABLE produtoCaderno CASCADE CONSTRAINTS;



/* Drop Sequences */

DROP SEQUENCE SEQ_produtoCaderno_idProduto;
DROP SEQUENCE SEQ_produto_idProduto;
DROP SEQUENCE SEQ_VendaCaderno_idVenda;
DROP SEQUENCE SEQ_Venda_idVenda;




/* Create Sequences */

CREATE SEQUENCE SEQ_produtoCaderno_idProduto INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_produto_idProduto INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_VendaCaderno_idVenda INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_Venda_idVenda INCREMENT BY 1 START WITH 1;



/* Create Tables */

CREATE TABLE ClienteCaderno
(
	Id_Cliente number(19,0) NOT NULL,
	nomeCliente varchar2(30),
	senhaCliente number,
	PRIMARY KEY (Id_Cliente)
);


CREATE TABLE ItemVendaCaderno
(
	preco float,
	idVenda number,
	idProduto number
);


CREATE TABLE produtoCaderno
(
	idProduto number NOT NULL UNIQUE,
	preco float,
	nomeProduto varchar2(30),
	altura number,
	largura number,
	quantidade number,
	PRIMARY KEY (idProduto)
);


CREATE TABLE VendaCaderno
(
	idVenda number NOT NULL UNIQUE,
	data date,
	vendedor varchar2(30),
	Id_Cliente number(19,0),
	PRIMARY KEY (idVenda)
);



/* Create Foreign Keys */

ALTER TABLE VendaCaderno
	ADD FOREIGN KEY (Id_Cliente)
	REFERENCES ClienteCaderno (Id_Cliente)
;


ALTER TABLE ItemVendaCaderno
	ADD FOREIGN KEY (idProduto)
	REFERENCES produtoCaderno (idProduto)
;


ALTER TABLE ItemVendaCaderno
	ADD FOREIGN KEY (idVenda)
	REFERENCES VendaCaderno (idVenda)
;



/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_produtoCaderno_idProduto BEFORE INSERT ON produtoCaderno
FOR EACH ROW
BEGIN
	SELECT SEQ_produtoCaderno_idProduto.nextval
	INTO :new.idProduto
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_produto_idProduto BEFORE INSERT ON produto
FOR EACH ROW
BEGIN
	SELECT SEQ_produto_idProduto.nextval
	INTO :new.idProduto
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_VendaCaderno_idVenda BEFORE INSERT ON VendaCaderno
FOR EACH ROW
BEGIN
	SELECT SEQ_VendaCaderno_idVenda.nextval
	INTO :new.idVenda
	FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_Venda_idVenda BEFORE INSERT ON Venda
FOR EACH ROW
BEGIN
	SELECT SEQ_Venda_idVenda.nextval
	INTO :new.idVenda
	FROM dual;
END;

/





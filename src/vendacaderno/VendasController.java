/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class VendasController implements Initializable {

    private Texto tx = new Texto();
    private TextField ProdutoTx;
    @FXML
    private TextField vendedorTx;
    @FXML
    private Button carrinhoBot;
    @FXML
    private Button comprarBot;
    @FXML
    private TableColumn<VendaCaderno, Integer> idVendaTab;
    @FXML
    private TableColumn<VendaCaderno, String> dataTab;
    @FXML
    private TableColumn<VendaCaderno, String> vendedorTab;
   @FXML
    private TableView<ProdutoCaderno> nomeProdutoTab;
   @FXML 
   private TableColumn<ProdutoCaderno, Integer> idProdutoTab;
    @FXML
    private TableColumn<ProdutoCaderno, String> nomeTab1;
    @FXML
    private TableColumn<ProdutoCaderno, Float> precoTab;
    private ObservableList<ProdutoCaderno> carrinho;
    private ObservableList<VendaCaderno> vendas;

    @FXML
    private TableView<VendaCaderno> VendasTab;
    //private Salvar sv = new Salvar();
    public static ArrayList<ProdutoCaderno> listaCarrinho= new ArrayList<>();
 
    @FXML
    private Button salvar;
    @FXML
    private ComboBox<ProdutoCaderno> produtoCb;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Carregar cg = new Carregar();
        carrinho= nomeProdutoTab.getItems();
        carrinho.clear();
        idProdutoTab.setCellValueFactory(new PropertyValueFactory<>("idProduto"));      
        nomeTab1.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));
        precoTab.setCellValueFactory(new PropertyValueFactory<>("preco"));
        //carrinho.clear();
        for(ProdutoCaderno c: listaCarrinho){
            carrinho.add(c);
        }
        //this.nomeProdutoTab.setItems(carrinho);
        
        
        vendas= VendasTab.getItems();
          
        idVendaTab.setCellValueFactory(new PropertyValueFactory<>("idVenda"));      
        dataTab.setCellValueFactory(new PropertyValueFactory<>("date"));
        vendedorTab.setCellValueFactory(new PropertyValueFactory<>("vendedor"));

        

        this.VendasTab.setItems(vendas);
        
        ArrayList<VendaCaderno> vendaCaderno = VendaCaderno.getAll();
        for(VendaCaderno vend: vendaCaderno){
            vendas.add(vend);}
        ArrayList<ProdutoCaderno> produtoCB = ProdutoCaderno.getAll();
        produtoCb.getItems().addAll(produtoCB);

    }    

    @FXML
    private void carrinhoBot(ActionEvent event) {
        ProdutoCaderno produtoCaderno = new ProdutoCaderno();
        produtoCaderno = (ProdutoCaderno) produtoCb.getSelectionModel().getSelectedItem();
       // produtoCaderno.getOne(Integer.parseInt(ProdutoTx.getText()));
        listaCarrinho.add(produtoCaderno);
        System.out.println(listaCarrinho + "Adicionado!");
       // nomeProdutoTab.refresh();
        att();
        
        //carrinho.add(produtoCaderno);
          tx.escreve(listaCarrinho);
    //    sv.salvarObject(produtoCaderno);*/

    }

    @FXML
    private void comprarBot(ActionEvent event) {        
        for(ProdutoCaderno caderno : FXMLDocumentController.pC){
            int id = getIdProduto(caderno);
            if(id != -1){
                caderno.setQuantidade(caderno.getQuantidade() - 1);
                caderno.update();
            }
        }
        carrinho.clear();
        listaCarrinho.clear();
        VendaCaderno vc = new VendaCaderno();
        java.util.Date dataNow = new java.util.Date();
        java.sql.Date dataSQL = new java.sql.Date(System.currentTimeMillis());
        vc.setDate(dataSQL);
        vc.setVendedor(vendedorTx.getText());
        vc.inserirVenda(vc);
        vendas.add(vc);
        VendasTab.refresh();
    //    File file1 = new File("carrinho.txt");
      //  file1.delete();
        File file = new File("salvar.ser");
        file.delete();
    }
    
    private int getIdProduto(ProdutoCaderno produto){
        ProdutoCaderno produto1;
        for(int i = 0; i<carrinho.size(); i++){
            produto1 = carrinho.get(i);
            if(produto1.getIdProduto()== produto.getIdProduto()){
               return i;
            }
        }
        return -1;
    }

    @FXML
    private void salvar(ActionEvent event) {
        new Salvar().salvarObject(listaCarrinho);
    }
    public void att(){
        carrinho= nomeProdutoTab.getItems();
        carrinho.clear();
        idProdutoTab.setCellValueFactory(new PropertyValueFactory<>("idProduto"));      
        nomeTab1.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));
        precoTab.setCellValueFactory(new PropertyValueFactory<>("preco"));
        //carrinho.clear();
        for(ProdutoCaderno c: listaCarrinho){
            carrinho.add(c);
        }
    }

    @FXML
    private void voltar(ActionEvent event) throws IOException {
                Parent root;
        try {
            System.out.println("trocando de tela");
            Stage stage = VendaCadernoExe.stage;

            root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException | IOException ex){
            throw new RuntimeException("Erro: Verifique o problema.", ex);
        }     


    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private TextField nomeProdutoTx;
    @FXML
    private TextField larguraTx;
    @FXML
    private TextField alturaTx;
    @FXML
    private TextField precoTx;
    @FXML
    private TextField quantidadeTx;
    @FXML
    private Button cadastrarBot;
    private ProdutoCaderno det;
    @FXML
    private TableColumn<ProdutoCaderno, String> nomeTab;
    @FXML
    private TableColumn<ProdutoCaderno, Float> precoTab;
    @FXML
    private TableColumn<ProdutoCaderno, Integer> alturaTab;
    @FXML
    private TableColumn<ProdutoCaderno, Integer> larguraTab;
    @FXML
    private TableColumn<ProdutoCaderno, Integer> quantidadeTab;
    public static ObservableList<ProdutoCaderno> pC;
    @FXML
    private TableView<ProdutoCaderno> tabela;
    @FXML
    private TableColumn<ProdutoCaderno, Integer> idTab;
    @FXML
    private Button deletarBot;
    @FXML
    private Button editarBot;
    @FXML
    private Button editarBot1;
    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
       pC= tabela.getItems();
        
        idTab.setCellValueFactory(new PropertyValueFactory<>("idProduto"));
        nomeTab.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));      
        precoTab.setCellValueFactory(new PropertyValueFactory<>("preco"));
        alturaTab.setCellValueFactory(new PropertyValueFactory<>("altura"));
        larguraTab.setCellValueFactory(new PropertyValueFactory<>("largura"));
        quantidadeTab.setCellValueFactory(new PropertyValueFactory<>("quantidade"));        

        this.tabela.setItems(pC);
        
        ArrayList<ProdutoCaderno> pcBanco = ProdutoCaderno.getAll();
        for(ProdutoCaderno pess: pcBanco){
            pC.add(pess);}

        // TODO
    }    

    @FXML
    private void cadastrarBot(ActionEvent event) {
         ProdutoCaderno pc = new ProdutoCaderno();
            pc.setNomeProduto(nomeProdutoTx.getText());
            pc.setPreco(Float.parseFloat(precoTx.getText()));
            pc.setAltura(Integer.parseInt(alturaTx.getText()));
            pc.setLargura(Integer.parseInt(larguraTx.getText()));
            pc.setQuantidade(Integer.parseInt(quantidadeTx.getText()));
            pC.add(pc);
            pc.inserirProduto(pc);

    }

    @FXML
    private void deleteBot(ActionEvent event) {
        tabela.getSelectionModel().getSelectedItem().delete();
        pC.remove(tabela.getSelectionModel().getSelectedItem());

    }

    @FXML
    private void editarBot(ActionEvent event) {
         det = tabela.getSelectionModel().getSelectedItem();
    editarBot1.setVisible(true);
    editarBot1.setDisable(false);
    cadastrarBot.setDisable(true);
    editarBot.setDisable(true);
    deletarBot.setDisable(true);
    ProdutoCaderno aux = det;
        nomeProdutoTx.setText(aux.getNomeProduto());
        precoTx.setText(String.valueOf(aux.getPreco()));
        alturaTx.setText(String.valueOf(aux.getAltura()));
        larguraTx.setText(String.valueOf(aux.getLargura()));
        quantidadeTx.setText(String.valueOf(aux.getQuantidade()));
    }

    @FXML
    private void editarBot1(ActionEvent event) {
                det = tabela.getSelectionModel().getSelectedItem();
            det.setNomeProduto(nomeProdutoTx.getText());
            det.setPreco(Float.parseFloat(precoTx.getText()));
            det.setAltura(Integer.parseInt(alturaTx.getText()));
            det.setLargura(Integer.parseInt(larguraTx.getText()));
            det.setQuantidade(Integer.parseInt(quantidadeTx.getText()));
            det.update();
                                pC = tabela.getItems();
                    pC.clear();
        idTab.setCellValueFactory(new PropertyValueFactory<>("idProduto"));
        nomeTab.setCellValueFactory(new PropertyValueFactory<>("nomeProduto"));      
        precoTab.setCellValueFactory(new PropertyValueFactory<>("preco"));
        alturaTab.setCellValueFactory(new PropertyValueFactory<>("altura"));
        larguraTab.setCellValueFactory(new PropertyValueFactory<>("largura"));
        quantidadeTab.setCellValueFactory(new PropertyValueFactory<>("quantidade"));        

        this.tabela.setItems(pC);
        
        ArrayList<ProdutoCaderno> pcBanco = ProdutoCaderno.getAll();
        for(ProdutoCaderno pess: pcBanco){
            pC.add(pess);}
    cadastrarBot.setDisable(false);
    editarBot.setDisable(false);
    deletarBot.setDisable(false);
    editarBot1.setVisible(false);
    editarBot1.setDisable(true);
    tabela.refresh(); 

    }

    @FXML
    private void trocaTela(ActionEvent event) {
                        Parent root;
        try {
            System.out.println("trocando de tela");
            Stage stage = VendaCadernoExe.stage;

            root = FXMLLoader.load(getClass().getResource("Vendas.fxml"));
            Scene scene = new Scene(root);

            stage.setScene(scene);

        } catch (NullPointerException | IOException ex){
            throw new RuntimeException("Erro: Verifique o problema.", ex);
        }     

    }
    
}

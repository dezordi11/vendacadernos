/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class VendaCaderno {

        private int idVenda;
        private Date date;
        private String vendedor;
        private ClienteCaderno cliente;

    public ClienteCaderno getCliente() {
        return cliente;
    }

    public void setCliente(ClienteCaderno cliente) {
        this.cliente = cliente;
    }
        
        
    public int getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(int idVenda) {
        this.idVenda = idVenda;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }
        
    public int inserirVenda(VendaCaderno vendaCaderno) {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO vendaCaderno"
                + "(idVenda, data, vendedor) VALUES"
                + "(seq_vendaCaderno_idVenda.nextval,?,?)";

        try {

            String generatedColumns[] = {"idVenda"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setDate(1, vendaCaderno.getDate());
            ps.setString(2, vendaCaderno.getVendedor());


            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into vendaCaderno table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            vendaCaderno.setIdVenda(chaveGerada);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idVenda;
    }    
            
public static ArrayList<VendaCaderno> getAll(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
        ArrayList<VendaCaderno> vendaLista = new ArrayList<>();
        String selectSQL = "SELECT * FROM VendaCaderno";
        Statement st;
        try{
            st = dbConnection.createStatement();
            ResultSet rs = st.executeQuery(selectSQL);
            while(rs.next()){
                VendaCaderno venCa = new VendaCaderno();
                venCa.setIdVenda(rs.getInt("idVenda"));
                venCa.setDate(rs.getDate("data"));
                venCa.setVendedor(rs.getString("vendedor"));
                              ClienteCaderno clienteCaderno = new ClienteCaderno();
                clienteCaderno.setId_Cliente(rs.getInt("id_Cliente"));
                clienteCaderno.getOne();
                venCa.setCliente(clienteCaderno);
                vendaLista.add(venCa);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            c.desconecta();
        }
        return vendaLista;
    }
    
        
}

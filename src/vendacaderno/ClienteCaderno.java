/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public class ClienteCaderno {
        private int id_Cliente;
        private String nomeCliente;
        private int senhaCliente;

    public int getId_Cliente() {
        return id_Cliente;
    }

    public void setId_Cliente(int Id_Cliente) {
        this.id_Cliente = Id_Cliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public int getSenhaCliente() {
        return senhaCliente;
    }

    public void setSenhaCliente(int senhaCliente) {
        this.senhaCliente = senhaCliente;
    }
       
       public void getOne(int i){
        this.id_Cliente = i;
        getOne();
    }
    public void getOne(){
        Conexao c = new Conexao();
        Connection dbConnection = c.getConexao();
            String selectSQL = "SELECT * FROM ClienteCaderno WHERE id_cliente = ?";
        
        PreparedStatement ps;
        try{
            ps = dbConnection.prepareStatement(selectSQL);
            ps.setInt(1, this.id_Cliente);
            
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                this.setId_Cliente(rs.getInt("id_cliente"));
                this.setNomeCliente(rs.getString("nomeCliente"));
                this.setSenhaCliente(rs.getInt("senhaCliente"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

 
}

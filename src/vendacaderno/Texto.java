/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vendacaderno;

import vendacaderno.ProdutoCaderno;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;


/**
 *
 * @author marcio
 */
public class Texto {
    private File f;
    private FileWriter fw;

    public Texto() {
        f = new File("carrinho.txt");      
        try {            
            fw = new FileWriter(f);
            fw.write("Arquivo gravado em : " + new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date()));
            }catch(FileNotFoundException e){
                System.out.println("Sem arquivo");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void escreve(ArrayList<ProdutoCaderno> produtos) {
        try {
            for(ProdutoCaderno produtoCaderno : produtos){
                fw.append("\n " + produtoCaderno.getNomeProduto());
            }
            fw.flush();//Envia os dados para o arquivo
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    protected void finalize() throws Throwable {
        try {
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        super.finalize(); 
    }
}
